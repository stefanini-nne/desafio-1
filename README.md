# Desafio 1 - Fazer um aplicativo de lista de tarefas

## Descrição:

Faça uma lista de tarefas que permita que o usuário adicione e remova itens. Seu aplicativo deve lembrar os itens toda vez que o usuário abrir o aplicativo.

## Requisitos de negócio

#### Como adicionar tarefas à lista
Seus usuários devem poder adicionar tarefas a lista ao digitar uma nova tarefa

#### Como deletar itens da lista de tarefas
Seus usuários devem poder deletar coisas da lista de tarefas assim que completá-las.

#### Como programar o botão “Reset”
Explicação: Para facilitar o uso do seu aplicativo, adicionaremos um botão para limpar as entradas colocadas pelo usuário na lista. 

#### Como ter mais uma lista de tarefas - Plus
Seus usuários devem poder ter mais de uma lista de tarefas

## Pontos a serem avaliados
- Qualidade do Código
- Organização do código
- Paradigma de Programação (Orientação a Objetos / Funcional)
- Uso de estruturas de dados
- Uso de padrões de projeto
- Documentação do Código / API
- Teste de Software
- Automatizadores/Aceleradores de código
- API
- Interface

## Como participar do Desafio

1. Forkar este reposítório
2. Realizar um clone em seu computador
3. Desenvolver a Solução
4. Ao final de cada dia uma entrega deve ser realizada em seu repositório
5. Submeter um pull request com a Solução, antes do Prazo Final.

## Plus

- Criação de uma plipeline(Gitlab, Jenkins, CircleCI)
- Deploy da aplicação(Heroku, Openshift, AWS, Azure, Digital Ocean)
- Automatização dos Testes(Selenium, Cypress, Protractor, Jest)



